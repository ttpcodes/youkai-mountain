import mqtt from 'mqtt'
import gpio, { promise as gpiop } from 'rpi-gpio'

const client = mqtt.connect(process.env.MOMIJI_MQTT_HOST, {
  password: process.env.MOMIJI_MQTT_PASSWORD,
  username: process.env.MOMIJI_MQTT_USERNAME,
})

gpio.setMode(gpio.MODE_BCM)
gpiop.setup(17, gpio.DIR_HIGH).then(() => {
  gpiop.write(17, false).catch((err) => {
    throw err
  })
}).catch((err) => {
  throw err
})
gpiop.setup(22, gpio.DIR_HIGH).catch((err) => {
  gpiop.write(22, false).catch((err) => {
    throw err
  })
  throw err
})
gpiop.setup(27, gpio.DIR_HIGH).catch((err) => {
  gpiop.write(27, false).catch((err) => {
    throw err
  })
  throw err
})

const ROOM_ENTERABLE = '/room_enterable'
const ROOM_OCCUPIED = '/room_occupied'

client.on('connect', () => {
  client.subscribe(ROOM_ENTERABLE, (err) => {
    if (err) {
      throw err
    }
  })
  client.subscribe(ROOM_OCCUPIED, (err) => {
    if (err) {
      throw err
    }
  })
})

client.on('message', (topic, message) => {
  switch (topic) {
    case ROOM_ENTERABLE:
      console.log('Received %s event: %s', ROOM_ENTERABLE, message)
      switch (message.toString()) {
        case 'off':
          console.log('Setting red on and green off.')
          gpiop.write(17, true).catch((err) => {
            throw err
          })
          gpiop.write(27, false).catch((err) => {
            throw err
          })
          break
        case 'on':
          console.log('Setting red off and green on.')
          gpiop.write(17, false).catch((err) => {
            throw err
          })
          gpiop.write(27, true).catch((err) => {
            throw err
          })
          break
      }
      break
    case ROOM_OCCUPIED:
      console.log('Received %s event: %s', ROOM_OCCUPIED, message)
      switch (message.toString()) {
        case 'off':
          console.log('Setting yellow on.')
          gpiop.write(22, true).catch((err) => {
            throw err
          })
          break
        case 'on':
          console.log('Setting yellow off.')
          gpiop.write(22, false).catch((err) => {
            throw err
          })
          break
      }
      break
  }
  console.log(topic + ': ' + message.toString())
})